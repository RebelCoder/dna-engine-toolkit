from engine_ui import Ui_MainWindow
from PyQt5 import QtWidgets
import sys


class DNAEngine:
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = QtWidgets.QApplication(sys.argv)
        self.MainWindow = QtWidgets.QMainWindow()

    def setup(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.MainWindow)

    def run(self):
        sys.exit(self.app.exec_())

    def display(self):
        self.MainWindow.show()
