from engine import DNAEngine

if __name__ == "__main__":
    Engine = DNAEngine()
    Engine.setup()
    Engine.display()
    Engine.run()
